# main projects
**cubelib** / *LIBRARY*<br>
Python library implements Minecraft protocol structures and provides high-level handles for it.
****
**I am a ghoul** / *SCRIPT*<br>
Python script makes dynamic discord status. Tokyo Ghoul reference. It's looks really fun in Russia / CIS.
****
**junkord** / *EXPERIMENT*<br>
Python experiments with private discord voice server API provided "as is". 
****
**kmtest**  / *LIBRARY*<br>
Python library providing high-level API to windows km-test v-ethernet adapter. Created around scapy.
****
**ku** / *LIBRARY*<br>
Python library created to make MITM proxies creation some easier.
****
**MCRP** / *LIBRARY, SCRIPT*<br>
Python library for creating Minecraft protocol MITM proxies. Python script for realtime Minecraft protocol packets sniffing and analyzing.
****
**chip-to-nfc** / *EXPERIMENT*<br>
Python experiment with main goal of making contact EMV cards be accessible contactless.
****
**myrror** / *LIBRARY,  ABORT*<br>
Python library for creating cross-version proxies on Minecraft protocol.
****
**skillcheckbot** / *SCRIPT*<br>
Python script for cheating in Dead by Daylight online game. Uses CV and wily algorithm to catch the moment a skillcheck appears and calc estimated time of touching the white zone.
****
**yggdrasil-server** / *SCRIPT*<br>
Python script for bypassing Mojang authentication. Allows to use official Mojang Minecraft Launcher without paid license.
